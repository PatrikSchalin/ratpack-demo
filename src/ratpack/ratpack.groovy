import static ratpack.groovy.Groovy.ratpack

import com.syve.ratpack.demo.Banner
import java.net.*

ratpack {

    serverConfig {
        port 8080
    }

    bindings {
        bind Banner
    }

    handlers {
        get {
            render "Hello Cloud World!"
        }
        get(":name") {
            render "Hello $pathTokens.name!"
        }
    }
}