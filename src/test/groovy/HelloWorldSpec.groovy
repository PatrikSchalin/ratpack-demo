import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.test.MainClassApplicationUnderTest
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class HelloWorldSpec extends Specification {

    // tag::GroovyScriptAUT[]
    @AutoCleanup
    @Shared
    GroovyRatpackMainApplicationUnderTest groovyScriptApplicationunderTest = new GroovyRatpackMainApplicationUnderTest()
    // end::GroovyScriptAUT[]

    @Unroll
    def 'Should render \'Hello Cloud World!\' from #type'() {
        when:
        def getText = aut.httpClient.getText()

        then:
        getText == 'Hello Cloud World!'

        where:
        aut                              | type
        groovyScriptApplicationunderTest | 'ratpack.groovy'
    }

    @Unroll
    def 'Should render \'Hello foo!\' from #type'() {
        when:
        def getText = aut.httpClient.getText('foo')

        then:
        getText == 'Hello foo!'

        where:
        aut                              | type
        groovyScriptApplicationunderTest | 'ratpack.groovy'
    }

    @Unroll
    def 'Should render \'Hello foobar!\' from #type'() {
        when:
        def getText = aut.httpClient.getText('foobar')

        then:
        getText == 'Hello foobar!'

        where:
        aut                              | type
        groovyScriptApplicationunderTest | 'ratpack.groovy'
    }

    @Unroll
    def 'Should render \'Hello feature-x!\' from #type'() {
        when:
        def getText = aut.httpClient.getText('feature-x')

        then:
        getText == 'Hello feature-x!'

        where:
        aut                              | type
        groovyScriptApplicationunderTest | 'ratpack.groovy'
    }
}