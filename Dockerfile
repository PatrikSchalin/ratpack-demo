FROM openjdk:8-jre-alpine

ARG VERSION
ARG VCS_COMMIT
ARG BUILD_DATE

ENV VERSION ${VERSION:-v0.0}


LABEL maintainer="Patrik Schalin <patrik.schalinX@tetrapak.com>"
LABEL com.tetrapak.build-date=$BUILD_DATE \
        com.tetrapak.name="smartsales-ratpack" \
        com.tetrapak.vcs-url="https://bitbucket.org/PatrikSchalin/ratpack-demo" \
        com.tetrapak.vcs-ref=$VCS_COMMIT \
        com.tetrapak.version=$VERSION \
        com.tetrapak.vendor="TetraPak AB" \
        com.tetrapak.schema-version="1.0.0-rc.1"

EXPOSE 8080

ENV RATPACK_PORT=8080
ENV RATPACK_DEVELOPMENT=false

COPY build/distributions/demo-app.zip .
RUN unzip demo-app.zip


ENTRYPOINT ["demo-app/bin/demo-app"]