#!/usr/bin/env groovy

pipeline {
    agent none
    stages {
        stage('Calculate Version') {
            agent {
                label 'docker'
            }
            steps {
                sh 'echo $(git describe --first-parent --dirty --tags) > VERSION'
                sh 'echo Building version: $(cat VERSION)'
            }
            post {
                success {
                    stash name: 'version', includes: 'VERSION'
                }
                cleanup {
                    deleteDir()
                }
            }
        }
        stage('Gradle Build') {
            agent {
                docker {
                    image 'openjdk:8-jdk-slim'
                }
            }
            steps {
                sh './gradlew clean assemble --stacktrace'
            }
            post {
                success {
                    stash name: 'distribution', includes: 'build/distributions/demo-app.zip'
                }
                cleanup {
                    deleteDir()
                }
            }
        }
        stage('Gradle Feature Test') {
            parallel {
                stage('Test-Suite 001') {
                    agent {
                        docker {
                            image 'openjdk:8-jdk-slim'
                        }
                    }
                    steps {
                        sh './gradlew test --stacktrace'
                    }
                    post {
                        always {
                            junit testResults: 'build/test-results/test/TEST-*.xml', allowEmptyResults: true
                        }
                        cleanup {
                            deleteDir()
                        }
                    }
                }
                stage('Test-Suite 002') {
                    agent {
                        docker {
                            image 'openjdk:8-jdk-slim'
                        }
                    }
                    steps {
                        sh './gradlew test --stacktrace'
                    }
                    post {
                        always {
                            junit testResults: 'build/test-results/test/TEST-*.xml', allowEmptyResults: true
                        }
                        cleanup {
                            deleteDir()
                        }
                    }
                }
                stage('Test-Suite 003') {
                    agent {
                        docker {
                            image 'openjdk:8-jdk-slim'
                        }
                    }
                    steps {
                        sh './gradlew test --stacktrace'
                    }
                    post {
                        always {
                            junit testResults: 'build/test-results/test/TEST-*.xml', allowEmptyResults: true
                        }
                        cleanup {
                            deleteDir()
                        }
                    }
                }
            }
        }
        stage('Docker Build') {
            agent {
                label 'docker'
            }
            environment {
                ACR_LOGIN_SERVER = 'smjenkinsacr.azurecr.io'
                YAHTZEE_RESOURCE_GROUP = 'TA-SMARTSALES-RGP-DEV-001'
                APPLICATION_NAME = 'ratpack'
            }
            steps {
                sh 'docker info'
                unstash name: 'distribution'
                unstash name: 'version'
                withCredentials([azureServicePrincipal('azure_service_principal')]) {
                    sh 'docker login $ACR_LOGIN_SERVER -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET'

                    sh """docker build --build-arg VERSION=\$(cat ./VERSION) \
                                        --build-arg VCS_COMMIT=${GIT_COMMIT} \
                                        --build-arg BUILD_DATE=${new Date().format('yyMMddHHmmssZ')} \\
                                        --tag $ACR_LOGIN_SERVER/$APPLICATION_NAME:latest \
                                        --tag $ACR_LOGIN_SERVER/$APPLICATION_NAME:\$(cat ./VERSION) ."""

                    sh 'docker push $ACR_LOGIN_SERVER/$APPLICATION_NAME:latest'
                    sh 'docker push $ACR_LOGIN_SERVER/$APPLICATION_NAME:$(cat ./VERSION)'
                }
            }
            post {
                cleanup {
                    deleteDir() /* clean up our workspace */
                }
            }
        }
        stage('Deploy') {
            agent {
                label 'docker'
            }
            when {
                branch 'master'
            }
            environment {
                ACR_LOGIN_SERVER = 'smjenkinsacr.azurecr.io'
                YAHTZEE_RESOURCE_GROUP = 'TA-SMARTSALES-RGP-DEV-001'
                APPLICATION_NAME = 'ratpack'
            }
            steps {
                unstash name: 'version'
                withCredentials([azureServicePrincipal('azure_service_principal')]) {
                    azureCLI commands: [[exportVariablesString: '', script: "az container create --resource-group ${YAHTZEE_RESOURCE_GROUP} --name smartsales-${APPLICATION_NAME} --image ${ACR_LOGIN_SERVER}/${APPLICATION_NAME}:latest --registry-username ${AZURE_CLIENT_ID} --registry-password ${AZURE_CLIENT_SECRET} --dns-name-label smartsales-${APPLICATION_NAME} --ports 8080"]], principalCredentialId: 'azure_service_principal'
                }
            }
            post {
                cleanup {
                    deleteDir() /* clean up our workspace */
                }
            }
        }
        stage('Deploy Test') {
            agent {
                label 'docker'
            }
            when {
                branch 'master'
            }
            steps {
                sh 'sleep 20s'
            }
            post {
                cleanup {
                    deleteDir() /* clean up our workspace */
                }
            }
        }
    }
}